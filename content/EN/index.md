title: "Hugo and GitLab Pages"

description: "A Hugo site built with GitLab Pages"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
